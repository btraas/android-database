package comp3717.bcit.ca.database;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.widget.SimpleCursorAdapter;

public class MainActivity
    extends ListActivity
{
    private static final String TAG = MainActivity.class.getName();
    private NamesOpenHelper     namesOpenHelper;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        final LoaderManager manager;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        namesOpenHelper = NamesOpenHelper.getInstance(getApplicationContext());
        adapter = new SimpleCursorAdapter(getBaseContext(),
                                          android.R.layout.simple_list_item_1,
                                          null,
                                          new String[]
                                          {
                                              "name",
                                          },
                                          new int[]
                                          {
                                              android.R.id.text1,
                                          },
                                          0);
        setListAdapter(adapter);
        manager = getLoaderManager();
        manager.initLoader(0, null, new NameLoaderCallbacks());
        init();
    }

    private void init()
    {
        final SQLiteDatabase db;
        final long           numEntries;

        db = namesOpenHelper.getWritableDatabase();
        numEntries = namesOpenHelper.getNumberOfNames(db);

        if(numEntries == 0)
        {
            db.beginTransaction();

            try
            {
                namesOpenHelper.insertName(db,
                                           "D'Arcy");
                namesOpenHelper.insertName(db,
                                           "Medhat");
                namesOpenHelper.insertName(db,
                                           "Albert");
                namesOpenHelper.insertName(db,
                                           "Jason");
                db.setTransactionSuccessful();
            }
            finally
            {
                db.endTransaction();
            }
        }

        db.close();
    }

    /*
    private void display()
    {
        final SQLiteDatabase db;
        final Cursor         cursor;

        db     = namesOpenHelper.getReadableDatabase();
        cursor = namesOpenHelper.getAllNames(db);

        while(cursor.moveToNext())
        {
            final String name;

            name = cursor.getString(0);
            Log.d(TAG, name);
        }

        cursor.close();
    }

    private void insert(final SQLiteDatabase db,
                        final String         name)
    {
        namesOpenHelper.insertName(db, name);
    }

    private void delete(final String name)
    {
        final SQLiteDatabase db;
        final int            rows;

        db   = namesOpenHelper.getWritableDatabase();
        rows = namesOpenHelper.deleteName(db, name);
    }

    private void updateList()
    {
        final ContentResolver contentResolver;

        contentResolver = getContentResolver();
        contentResolver.notifyChange(NameContentProvider.CONTENT_URI, null);
    }
    */

    private class NameLoaderCallbacks
        implements LoaderCallbacks<Cursor>
    {
        @Override
        public Loader<Cursor> onCreateLoader(final int    id,
                                             final Bundle args)
        {
            final Uri          uri;
            final CursorLoader loader;

            uri    = NameContentProvider.CONTENT_URI;
            loader = new CursorLoader(MainActivity.this, uri, null, null, null, null);

            return (loader);
        }

        @Override
        public void onLoadFinished(final Loader<Cursor> loader,
                                   final Cursor         data)
        {
            adapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(final Loader<Cursor> loader)
        {
            adapter.swapCursor(null);
        }
    }
}